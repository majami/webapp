<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once "user/user.service.php";
include_once "bonus/bonus.dal.php";
include_once "logs/logger.php";
include_once "exceptions/forbidden.exception.php";
include_once "exceptions/missing_parameters.exception.php";

class BonusService {

  private Logger $logger;
  private BonusDal $dal;
  private UserService $user_service;

  public function __construct() {
    $this->logger = new Logger("BonusService");
    $this->dal = new BonusDal();
    $this->user_service = new UserService();
  }

  public function create_bonus(int $user_id, string $name, string $type, float $value, ?int $principal) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to create boni");
    if($user->get_role() == "ADMIN" && null == $principal)
      throw new MissingParametersException("Admins have to provide a principal to create a bonus.");
    if($user->get_role() != "ADMIN")
      $principal = $user->get_principal();

    return $this->dal->store_bonus($name, $type, $value, $principal);
  }

  public function fetch_boni(int $user_id, ?int $principal) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to view boni.");
    if($user->get_role() == "ADMIN" && null == $principal)
      throw new MissingParametersException("Admins have to provide a principal to view boni.");
    if($user->get_role() != "ADMIN")
      $principal = $user->get_principal();

    return $this->dal->fetch_boni($principal);
  }

  public function assign_bonus(int $user_id, int $id, int $bonus) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to assign boni.");
    $this->dal->assign_bonus($id, $bonus);
  }

}

 ?>
