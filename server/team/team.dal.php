<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "logs/logger.php";
require_once "util/database.util.php";
require_once "team/team.dto.php";
require_once "exceptions/not_found.exception.php";
require_once "exceptions/not_stored.exception.php";

class TeamDal {

  public function __construct() {
    $this->logger = new Logger("TeamDal");
  }

  public function store_team(int $user_id, int $principal, string $name) {
    $mysqli = create_db_connection();
    $id = fetch_id($mysqli, "team", $this->logger);
    $query = "INSERT INTO ".TABLE_PREFIX."team ".
              "(id,principal,name) VALUES (".
              $id.",".
              $principal.",".
              "'".$mysqli->real_escape_string($name)."'".
              ")";
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false === $result) {
      throw new NotStoredException("Team could not be added.");
    }
    return new Team($id, $principal, $name);
  }

  public function fetch_teams(?int $principal) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."team";
    if(null !== $principal) {
      $query = $query." WHERE principal = ".$principal;
    }
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false === $result) {
      throw new NotFoundException("Teams could not be fetched.");
    }
    $teams = array();
    while($row = $result->fetch_assoc()) {
      $teams[] = new Team(intval($row["id"]), intval($row["principal"]), $row["name"]);
    }
    return $teams;
  }

  public function fetch_team_members(int $team_id) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."team_members ".
              "WHERE team = ".$team_id;
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false == $result) {
      throw new NotFoundException("Team members query failed");
    }
    $team_members = array();
    while($row = $result->fetch_assoc()) {
      $team_members[] = intval($row["user"]);
    }
    return $team_members;
  }

  public function fetch_assigned_teams(int $user_id) {
    $mysqli = create_db_connection();
    $query = "SELECT t.id as id, t.principal as principal, t.name as name FROM ".TABLE_PREFIX."team t ".
              "INNER JOIN ".TABLE_PREFIX."team_members m ON m.team = t.id ".
              "WHERE m.user = ".$user_id;
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false == $result) {
      throw new NotFoundException("Assigned teams query failed.");
    }
    $teams = array();
    while($row = $result->fetch_assoc()) {
      $teams[] = new Team(intval($row["id"]), intval($row["principal"]), $row["name"]);
    }
    return $teams;
  }

  public function assign_team(int $asignee, int $team) {
    $mysqli = create_db_connection();
    $query = "INSERT INTO ".TABLE_PREFIX."team_members ".
              "(user,team) VALUES ($asignee,$team)";
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotStoredException("Failed to insert team assignemt.");
  }

  public function fetch_team(int $team_id) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."team WHERE id = $team_id";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotFoundException("Team could not be found.");
    $row = $result->fetch_assoc();
    return new Team(intval($row["id"]), intval($row["principal"]), $row["name"]);
  }

}

 ?>
