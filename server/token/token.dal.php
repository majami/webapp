<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'util/database.util.php';
include_once 'token/token.dto.php';
include_once 'config.inc.php';
include_once 'logs/logger.php';
include_once 'exceptions/not_stored.exception.php';
include_once 'exceptions/not_deleted.exception.php';

class TokenDal {

  private $logger;

  public function __construct() {
    $this->logger = new Logger("TokenDal");
  }

  public function store_token(Token $token) {
    $mysqli = create_db_connection();
    $query = "INSERT INTO ".TABLE_PREFIX."token (token,expiry_date,user) ".
                    "VALUES ('".$token->get_token()."','".
                    $token->get_expiry_date()->format('Y-m-d H:i:s')."',".$token->get_user_id().")";
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);
    if($result == false) {
      $message = "Token not stored into DB because SQL failed.";
      $this->logger->error($message);
      throw new NotStoredException($message);
    }
    $this->logger->debug("New token stored.");
  }

  public function fetch_token(string $token) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."token".
                    " WHERE token like '".$token."'";
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);
    if($result != false && $row = $result->fetch_assoc()) {
      $expiry_date = new DateTime($row["expiry_date"]);
      $found_token = new Token($row["token"], $expiry_date, $row["user"]);
      return $found_token;
    } else {
      $msg = "Token not found: ".$token;
      $this->logger->warn($msg);
      throw new NotFoundException($msg);
    }
  }

  public function delete_token(string $token) {
    $mysqli = create_db_connection();
    $query = "DELETE FROM ".TABLE_PREFIX."token WHERE token like '".$token."'";
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);
    if($result == false) {
      $msg = "Token not deleted.";
      $this->logger->error($msg);
      throw new NotDeletedException($msg);
    }
  }

}

 ?>
