<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class User {

  private int $id;
  private string $username;
  private string $password;
  private string $role = "ATHLETE";
  private ?string $forename = "";
  private ?string $name = "";
  private ?int $principal = null;

  private Logger $logger;

  public function __construct($id, $username, $unhashed_password) {
    $this->id = $id;
    $this->username = $username;
    /*
    Storing the unhashed password because this one is loaded
    from database, where only hashed passwords are stored.
    */
    $this->password = $unhashed_password;
    $this->logger = new Logger("User");
  }

  public function get_id() {
    return $this->id;
  }

  public function get_username() {
    return $this->username;
  }

  public function set_username($username) {
    $this->username = $username;
  }

  public function set_password($password) {
    $this->password = password_hash($password, PASSWORD_BCRYPT);
  }

  public function same_password($to_compare) {
    return password_verify($to_compare, $this->password);
  }

  public function get_password() {
    return $this->password;
  }

  public function set_role(string $role) {
    $this->role = $role;
  }

  public function get_role() {
    return $this->role;
  }

  public function set_forename(?string $forename) {
    $this->forename = $forename;
  }

  public function get_forename() {
    return $this->forename;
  }

  public function set_name(?string $name) {
    $this->name = $name;
  }

  public function get_name() {
    return $this->name;
  }

  public function set_principal(?int $principal) {
    $this->principal = $principal;
  }

  public function get_principal() {
    return $this->principal;
  }

}

?>
