<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'util/database.util.php';
include_once 'config.inc.php';
include_once 'exceptions/not_found.exception.php';
include_once 'logs/logger.php';
include_once 'user/user.dto.php';

class UsersDal {

  private Logger $logger;

  public function __construct() {
    $this->logger = new Logger("UsersDal");
  }

  /*
  Function just for login purposes.
  */
  public function fetch_user(string $username) {
    $mysqli = create_db_connection();
    $query = "SELECT id,username,password,role,principal".
              " FROM ".TABLE_PREFIX."user ".
              "WHERE username = '".$mysqli->real_escape_string($username)."'";
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);

    if($result != false && $row = $result->fetch_assoc()) {
      $user = new User($row["id"], $row["username"], $row["password"]);
      if($row["role"] != null) $user->set_role($row["role"]);
      if($row["principal"] != null) $user->set_principal($row["principal"]);
      return $user;
    } else {
      throw new NotFoundException("Requested admin not in database.");
    }
  }

  public function fetch_user_by_id(int $user_id) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."user WHERE id = ".$user_id;
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);

    if($result != false && $row = $result->fetch_assoc()) {
      $user = new User($row["id"], $row["username"], $row["password"]);
      $user->set_role($row["role"]);
      $user->set_forename($row["forename"]);
      $user->set_name($row["name"]);
      if(null !== $row["principal"]) $user->set_principal($row["principal"]);
      return $user;
    } else {
      throw new NotFoundException("Requested user not in database.");
    }
  }

  public function update_user(User $user) {
    $mysqli = create_db_connection();
    $query = "UPDATE ".TABLE_PREFIX."user ".
              "SET username = '"
              .$mysqli->real_escape_string($user->get_username())."', ".
              "password = '".$user->get_password()."' ".
              "WHERE id = ".$user->get_id();
    $this->logger->debug("Execute query ".$query);
    $result = $mysqli->query($query);
    if($result == false) {
      $msg = "User update failed.";
      $this->logger->error($msg);
      throw new NotStoredException($msg);
    }
  }

  private function fetch_id($mysqli) {
    $query = "SELECT MAX(id) as max_id FROM ".TABLE_PREFIX."user";
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);
    if($result == false) {
      $msg = "Not able to select maximum id from users.";
      $this->logger->error($msg);
      throw new NotFoundException($msg);
    }
    $row = $result->fetch_assoc();
    $max_id = intval($row["max_id"]);
    $new_id = $max_id + 1;
    return $new_id;
  }

  public function create_user(User $user) {
    $mysqli = create_db_connection();
    $id = $this->fetch_id($mysqli);
    $query = "INSERT INTO ".TABLE_PREFIX."user ".
              "(id,username,password,role,forename,name,principal) ".
              "VALUES (".
              $id.",".
              "'".$mysqli->real_escape_string($user->get_username())."',".
              "'".$mysqli->real_escape_string($user->get_password())."',".
              "'".$mysqli->real_escape_string($user->get_role())."',".
              "'".$mysqli->real_escape_string($user->get_forename())."',".
              "'".$mysqli->real_escape_string($user->get_name())."',".
              $user->get_principal().
              ")";
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);
    if($result == false) {
      $this->logger->error("User not stored.");
      throw new NotStoredException();
    }
    $ret_val = new User($id,$user->get_username(),$user->get_password());
    $ret_val->set_role($user->get_role());
    $ret_val->set_forename($user->get_forename());
    $ret_val->set_name($user->get_name());
    $ret_val->set_principal($user->get_principal());
    return $ret_val;
  }

  public function fetch_users(int $principal) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."user WHERE principal = ".$principal;
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);
    if($result == false) {
      throw new NotFoundException("No users found for principal ".$principal);
    }
    $ret_val = array();
    while($row = $result->fetch_assoc()) {
      $user = new User(intval($row["id"]),$row["username"],"");
      $user->set_role($row["role"]);
      $user->set_forename($row["forename"]);
      $user->set_name($row["name"]);
      $user->set_principal(intval($row["principal"]));
      $ret_val[] = $user;
    }
    return $ret_val;
  }

  public function change_username(int $user_id, string $new_username) {
    $mysqli = create_db_connection();
    $query = "UPDATE ".TABLE_PREFIX."user SET username = '$new_username' WHERE id = $user_id";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotStoredException("Username could not be updated.");
  }

  public function change_forename(int $user_id, string $new_forename) {
    $mysqli = create_db_connection();
    $query = "UPDATE ".TABLE_PREFIX."user SET forename = '$new_forename' WHERE id = $user_id";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotStoredException("Forename could not be updated.");
  }

  public function change_name(int $user_id, string $new_name) {
    $mysqli = create_db_connection();
    $query = "UPDATE ".TABLE_PREFIX."user SET name = '$new_name' WHERE id = $user_id";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotStoredException("Name could not be updated.");
  }

  public function change_profile_picture(int $user_id, string $image) {
    $mysqli = create_db_connection();
    $query = "UPDATE ".TABLE_PREFIX."user SET picture = '"
                .$mysqli->real_escape_string($image)."' WHERE id = $user_id";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotStoredException("Profile picture could not be stored.");
  }

  public function fetch_profile_picture(int $owner) {
    $mysqli = create_db_connection();
    $query = "SELECT picture FROM ".TABLE_PREFIX."user ".
              "WHERE id = $owner";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotFoundException("Profile picture not found.");
    $row = $result->fetch_assoc();
    $image = $row["picture"];
    if(null == $image || "" == $image)
      throw new NotFoundException("No profile picture stored for this user.");
    $this->logger->debug("Got picutre: ".explode(",", $image)[0]);
    return stripslashes($image);
  }

}

?>
