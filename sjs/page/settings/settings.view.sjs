/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class SettingsView {

  constructor() {
    this.log = new Logger("SettingsView");
    this.controller = new SettingsController();
    this.utils = new Utils();

    this.log.debug("Creating settings view");
    $("title").text("Einstellungen");
    history.pushState({page:0}, $("title").text(), "?p=settings");
    let ref = this;
    let templates = [];
    templates.push({container: "#app", name: "settings/settings.html"});
    templates.push({container: "#modals", name: "settings/settings-modals.html"});
    this.utils.loadMultipleTemplates(templates, function() {
      ref._buildPage();
    });
  }

  _buildPage() {
    let ref = this;
    this.controller.fetchUserData(function(user) {
      $("#username").val(user.username);
      $("#forename").val(user.forename);
      $("#lastname").val(user.name);
      ref._hookListeners();
      ref._setupProfilePictureModal(user.id);
      ref.controller.fetchProfilePicture(user.id, function(image) {
        $("#profile-pic").attr("src", image);
      }, function() {
        ref.log.warn("No profile picture found, taking default.");
      })
    }, function() {
      alert("Benutzerdaten konnten nicht geladen werden.");
      new OverviewView();
    });
  }

  _hookListeners() {
    this._hookNameValidators();
    this._hookSaveNameListener();
    this._hookPasswordValidators();
    this._hookSavePasswordListener();
  }

  _hookNameValidators() {
    let ref = this;
    $("#username").on("keyup", function() {
      ref.log.debug("Validating username...");
      let username = $("#username");
      let button = $("#save-name");
      if(username.val().trim() == "") {
        username.addClass("is-invalid");
        button.attr("disabled", "");
      } else {
        username.removeClass("is-invalid");
        button.removeAttr("disabled");
      }
    })
  }

  _hookSaveNameListener() {
    let ref = this;
    let button = $("#save-name");
    button.click(function(e) {
      e.preventDefault();
      let forename = $("#forename");
      let lastname = $("#lastname");
      let username = $("#username");
      ref.utils.addSpinner(button, false);
      ref.controller.changeUserData(username.val(), forename.val(), lastname.val(), function() {
        ref.utils.removeSpinner(button, false);
      }, function() {
        ref.utils.removeSpinner(button, false);
        alert("Namen konnten nicht gespeichert werden.");
      });
    });
  }

  _hookPasswordValidators() {
    let newPassword = $("#new-password");
    let confirmPassword = $("#confirm-password");
    let button = $("#save-password");
    let newPasswordValid = false;
    let confirmPasswordValid = false;

    newPassword.on("keyup", function() {
      if(newPassword.val().trim() != "") {
        newPassword.addClass("is-valid");
        newPasswordValid = true;
        if(confirmPasswordValid) {
          button.removeAttr("disabled");
        }
      } else {
        newPasswordValid = false;
        newPassword.removeClass("is-valid");
        button.attr("disabled", "");
      }
    });

    confirmPassword.on("keyup", function() {
      if(confirmPassword.val().trim() != "" && newPasswordValid && confirmPassword.val() == newPassword.val()) {
        confirmPassword.addClass("is-valid");
        confirmPasswordValid = true;
        button.removeAttr("disabled");
      } else {
        confirmPasswordValid = false;
        confirmPassword.removeClass("is-valid");
        button.attr("disabled", "");
      }
    });
  }

  _hookSavePasswordListener() {
    let button = $("#save-password");
    let newPassword = $("#new-password");
    let ref = this;
    button.click(function(e) {
      e.preventDefault();
      ref.utils.addSpinner(button, false);
      ref.controller.changePassword(newPassword.val(), function() {
        ref.utils.removeSpinner(button, false);
      }, function() {
        ref.utils.removeSpinner(button, false);
        alert("Passwort konnte nicht geändert werden.");
      });
    });
  }

  _setupProfilePictureModal(userId) {
    let modal = $("#change-profile-picture-modal");
    let saveButton = modal.find("button.save");
    let deleteButton = modal.find("button.delete");
    let imageInput = modal.find("input[name=image]");

    let ref = this;
    deleteButton.click(function(e) {
      e.preventDefault();
      ref.utils.addSpinner(deleteButton, true);
      ref.controller.changeProfilePicture("", function() {
        ref.utils.removeSpinner(deleteButton, true);
        modal.modal('hide');
      }, function() {
        ref.utils.removeSpinner(deleteButton, true);
        alert("Profilbild konnte nicht entfernt werden.");
      });
    });
    saveButton.click(function(e) {
      e.preventDefault();
      ref.log.debug("Image selection input has value: " + imageInput.val());
      if(imageInput.val() == "" || imageInput.val() == undefined) {
        alert("Bitte Profilbild auswählen.");
      } else {
        ref.utils.addSpinner(saveButton, true);
        ref.utils.getImageDataURL(imageInput, function(image) {
          ref.controller.changeProfilePicture(image, function() {
            ref.utils.removeSpinner(saveButton, true);
            $("#profile-pic").attr("src", image);
            imageInput.val(null);
            modal.modal('hide');
          }, function() {
            ref.utils.removeSpinner(saveButton, true);
            alert("Profilbild konnte nicht geändert werden.");
          })
        });
        ref.controller.changeProfilePicture("", function() {
          ref.utils.removeSpinner(deleteButton, true);
          modal.modal('hide');
        }, function() {
          ref.utils.removeSpinner(deleteButton, true);
          alert("Profilbild konnte nicht entfernt werden.");
        });
      }
    });
  }

}
